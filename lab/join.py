# -*- coding: utf-8 -*-

import json
import unittest

from rsmr import MapReduce


def map(item):
    pass


def reduce(key, values):
    pass


def read_data(filename):
    data = open(filename)
    return json.load(data)


def join_records():
    input_data = read_data('records.json')
    mapper = MapReduce(map, reduce)
    results = mapper(input_data)
    return results


class JoinTest(unittest.TestCase):

    def test_results(self):
        expected = sorted(read_data('join.json'))
        current = sorted(join_records())
        self.assertListEqual(expected, current)


if __name__ == '__main__':
    unittest.main()
